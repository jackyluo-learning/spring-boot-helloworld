#!/usr/bin/env bash

set -e

HARBOR_REGISTRY='registry.cn-guangzhou.aliyuncs.com/fake-but-real-docker-images'

usage()  {
cat << EOF
Usage: build_image.sh tag
     tag: image tag, (e.g: 1.0)
EOF
}

if [[ $# -lt 1 ]]; then
    usage
    exit 1
fi


service=docker-images
tag=$1

cur_dir=`dirname $0`

echo building image ${service}:${tag}
docker build -t ${HARBOR_REGISTRY}/${service}:${tag} -f ${cur_dir}/Dockerfile ${cur_dir}/..
docker push ${HARBOR_REGISTRY}/${service}:${tag}