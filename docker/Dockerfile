# 使用OpenJDK 8官方镜像作为基础镜像
FROM openjdk:8-jdk-alpine as build

# 设置工作目录
WORKDIR /spring-boot-helloworld

# 复制Gradle相关文件
COPY gradle/wrapper gradle/wrapper
COPY gradlew .
COPY build.gradle .
COPY src src

# 构建应用，跳过测试
RUN chmod +x ./gradlew && ./gradlew build -x test

# 使用OpenJDK 8运行时基础镜像
FROM openjdk:8-jre-alpine

# Alpine Linux没有yum包管理器，也不需要安装OpenJDK（因为基础镜像已经是Java环境）
# 以下设置环境变量，配置时区和中文支持
ENV LANG=C.UTF-8

# 设置工作目录
WORKDIR /spring-boot-helloworld

# 将构建生成的jar文件和启动脚本复制到镜像中
COPY --from=build /spring-boot-helloworld/build/libs/spring-boot-helloworld-0.0.1-SNAPSHOT.jar ./spring-boot-helloworld.jar
COPY docker/service.sh ./bin/service.sh

# 添加脚本的执行权限
RUN chmod +x ./bin/service.sh

# 设置时区
RUN apk add --no-cache tzdata && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" > /etc/timezone && \
    apk del tzdata

ENTRYPOINT ["./bin/service.sh", "start", "spring-boot-helloworld.jar", "--no-daemon"]
